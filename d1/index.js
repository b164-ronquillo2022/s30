const express = require ('express');
//Mongoose is a package that allows creation of Schemas to model our data structures
//Also has access to a number of methods for manipulation our database
const mongoose = require("mongoose");
const app = express();
const port = 3001;
//MongoDB Atlas Connection
//Connect to the database by passing in your connection string, remember to replace the password and database names with actual values.
//when we want to use local mongoDB/robo3t
//mongoose.connect("mongodb://localhost:27017/databaseName")


mongoose.connect("mongodb+srv://admin:admin@cluster0.alg8k.mongodb.net/batch164_to-do?retryWrites=true&w=majority",
		{
    		useNewUrlParser: true,
    		useUnifiedTopology: true
		}

)
//Due to updates in mongodb drivers that allow connection to it, the default connection string is being flagged as an error
//By default a warning will be displayed in the terminal when the app is running, but this will not prevent mongoose from being used in the app.
//to prevent/avoid any current and future errors while connecting to Mongo DB, add the userNewUrlParser and useUnifiedTopology.

//Set notifications for connection success or failure

let db = mongoose.connection;
//console.error.binde(console) allows us to print errors in the browser console and in the terminal
//"connection error" is the message that will display if an error is encountered.
db.on("error", console.error.bind(console, "connection error"));
//connection successful message

db.once("open", () => console.log("we're connected to the cloud database"))

app.use(express.json());
app.use(express.urlencoded({extended:true}));


//Mongoose Schemas
//Schemas determine the structure of the documents to be written in the database
//act as blueprint to our data
//use Schema() constructor of the mongoose module to crate a new schema object

const taskSchema =  new mongoose.Schema({
	name: String,
	status: {
		type: String,
		//Default values are the predefined values for a field if we dont put any value
		default: "pending"
	}
})

const Task = mongoose.model("Task", taskSchema);

//Routes/endpoints
//Creating a new task
//Business logic
/*
1. Add a functionality to check if there are duplicates tasks
-if the task already exist in the database. we return error
-if the task doesnt exist in the database, we add it in the database
   a. The task data will be coming from the request body.
   b. Create a new task object with field/property
   c.save the new object to our database.
   */

   app.post("/tasks",(req, res)=> {
   	Task.findOne({ name: req.body.name}, (err, result) => {
         //if a document was found and the documents name matches the information
         //sent via the client
         if(result != null && result.name == req.body.name){
         	return res.send("Duplicate task Found");

         }else {
         	let newTask = new Task({
         		name: req.body.name
         	});

         	newTask.save((saveErr, savedTask) => {
               //if there are error in saving
               if(saveErr){
               	return console.error(saveErr)
               }else {
               	return res.status(201).send("New task created")
               }
         	})
         }
   	})
   })

   //Get all tasks
   //Business Logic
   /*
   1. find/retrieve all the documents
   2. if an error is encountered, print the error
   3. if no error are found, send a success status back*/
   app.get("/tasks", (req, res) => {
   	Task.find({}, (err, result) => {
   		if(err) {
   			return console.log(err);
   		} else {
   			return res.status(200).json({
   				dataFromMDB: result
   			})
   		}
   	})
 })

//sol 1: create user schema

const userSchema = new mongoose.Schema({
    username: {
        type: String,
        required: [true, `Enter Name.`]
    },
    password: {
        type: String,
        required: [true, 'Enter Password.']
    }
    
})

//sol 2: create User model
const userModel = mongoose.model(`userModel`, userSchema);

//sol 3-4: Post /signup

app.post("/signup", (req, res) => {
    

    userModel.findOne({username: req.body.username}).then((result, err) => {
            console.log(result)
     
            
            if (result != null && result.username == req.body.username){
                return res.send(`Is already registered.`)
            } else {    
                userModel.create(req.body);
                res.send(`User ${req.body.username} registered you can now Login.`);
            }

        
    })
})

//sol 5-6: return all users

   app.get("/users", (req, res) => {
   	userModel.find({}, (err, result) => {
   		if(err) {
   			return console.log(err);
   		} else {
   			return res.status(200).json({
   				dataFromMDB: result
   			})
   		}
   	})
 })


   	
app.listen(port, ()=> console.log(`Server running at port ${port}`) );

/*

Activity:
1. Create a User schema.
2. Create a User model.
3. Create a POST route that will access the "/signup" route that will create a user.
4. Process a POST request at the "/signup" route using postman to register a user.
5. Create a GET route that will return all users.
6. Process a GET request at the "/users" route using postman.
7. Create a git repository named S30.
8. Initialize a local git repository, add the remote link and push to git with the commit message of s30 Activity.
9. Add the link in Boodle named Express js Data Persistence via Mongoose ODM.


Business Logic in Creating a user "/signup"
1. Add a functionality to check if there are duplicates tasks
        - if the user already exists, return error Or "Already registered"
        - if the user does not exist, we add it on our database
                1. The user data will be coming from the req.body
                2. Create a new User object with a "username" and "password" fields/properties
                3. Then save, and add an error handling
*/